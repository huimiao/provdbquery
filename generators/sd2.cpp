#include <cmath>
#include <ctime>
#include <iostream>
#include <fstream>
#include <random>
#include <set>
#include <string>
#include <vector>

#include <gsl/gsl_sf_zeta.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

using namespace std;

// when one doesn't know the population
// assume the population is infinity, generalized harmonic number is 
// approximated as zeta function
double get_pdf_zipf(int rank, double skew) {
  // use zeta function, one can approximate the zipf pmf
  return pow(rank, -1 * skew) / gsl_sf_zeta(skew);
}

// when one knows the population
// calculate the generalized harmonic number
set<int> gen_input_entities_zipf(int num_entities, int size, double skew) {
  set<int> result;
  if (size == 0) return result;
  // popularity for existing entities, the later entities, got higher rank
  double popularities[num_entities];
  for (int i = 0; i < num_entities; i++) {
    int rank = num_entities - i;
    popularities[i] = get_pdf_zipf(rank, skew); // use the infinity population
  }
  gsl_rng* input_entities_generator = gsl_rng_alloc(gsl_rng_mt19937);
  gsl_ran_discrete_t* input_entities_distr = 
      gsl_ran_discrete_preproc(num_entities, popularities);
  while (result.size() < size) {
    int e = gsl_ran_discrete(input_entities_generator, input_entities_distr);
    result.insert(e);
  }
  gsl_ran_discrete_free(input_entities_distr);
  gsl_rng_free(input_entities_generator);
  return result;
}

int main(int argc, char** argv) {
  if (argc < 6) {
    cout << "usage: ./sd2 out_path a k l s [in_skew] [in_degree]" << endl;
    return 0;
  }

  // sd2 parameters
  string path(argv[1]);
  // alpha, Dirichlet concentration parameter
  double a = stod(argv[2]);
  // kinds of activities
  int k = stoi(argv[3]);
  // # of activities of a segment
  int l = stoi(argv[4]);
  // # of segments
  int s = stoi(argv[5]);

  cout << "path: \t\t\t\t" << path << endl;
  cout << "(a, k, l, s): \t\t\t" << a << ", " << k << ", " << l << ", " << s
       << endl;

  // sd1 parameters
  // input entity skew
  double in_skew = 1.5;
  // activity entities
  int in_mean = 2;
  int out_mean = 2;

  if (argc == 7) in_skew = stod(argv[6]);
  if (argc == 8) in_mean = stoi(argv[7]);

  cout << "in_skew, in_, out_mean: \t" << in_skew << ", " << in_mean << ", "
       << out_mean << endl;

  gsl_rng_env_setup();
  gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
  // activity - input poisson mean
  default_random_engine in_num_generator(time(0));
  poisson_distribution<int> in_num_distr(in_mean);
  // activity - output poisson mean
  default_random_engine out_num_generator(time(0) + rand());
  poisson_distribution<int> out_num_distr(out_mean);

  // get TM: transition matrix
  double alpha[k];
  for (int i = 0; i < k; i++) alpha[i] = a;
  cout << "transition matrix: " << endl;
  vector<vector<double>> trans_matrix;
  for (int i = 0; i < k; i++) {
    double theta[k];
    gsl_ran_dirichlet(r, k, alpha, theta);
    for (int j = 0; j < k; j++) cout << round(theta[j] * 100) / 100 << " ";
    cout << endl;
    trans_matrix.push_back(vector<double>(theta, theta + k));
  }

  fstream fout;
  fout.open(path, fstream::out); 

  // use the transition matrix to derive a list of activities
  cout << "gen segments: " << endl;
  for (int t = 0; t < s; t++) {
    // each segment
    int curr_label = 0;
    int num_entities = 0;
    
    // output edges
    vector<pair<int, int>> activity_labels;      // A x L
    vector<pair<int, int>> used;                 // A x E
    vector<pair<int, int>> was_generated_by;     // E x A

    for (int i = 0; i < l; i++) {
      cout  << curr_label << " ";

      int curr_activity = i;
      int in_size = 1 + in_num_distr(in_num_generator); 
      int out_size = 1 + out_num_distr(out_num_generator);

      activity_labels.push_back(make_pair(curr_activity,curr_label));
      
      if (num_entities < in_size) num_entities = in_size;  // init
      auto in_es = gen_input_entities_zipf(num_entities, in_size, in_skew);
      cout << "in(";
      for (int input_entity : in_es) {
        cout << input_entity << " ";
        used.push_back(make_pair(curr_activity, input_entity));
      }
      cout << ");";
      int output_entity = num_entities;
      cout << "out(";
      for (; output_entity < num_entities + out_size; output_entity++) {
        cout << output_entity << " ";
        was_generated_by.push_back(make_pair(output_entity, curr_activity));
      }
      cout << ") ";
      num_entities += out_size;

      // change label
      gsl_ran_discrete_t* transition =
          gsl_ran_discrete_preproc(k, trans_matrix[curr_label].data());
      curr_label = gsl_ran_discrete(r, transition);
      gsl_ran_discrete_free(transition);
    }
    cout << endl;
    // write it out
    fout << "segment: [" << t << "]" << endl;
    fout << "vertex id: label" << endl;
    int entity_label = k;
    for (auto activity : activity_labels) 
      fout << activity.first + num_entities << ": " << activity.second << endl;
    for (int i = 0; i < num_entities; i++)
      fout << i << ": " << entity_label << endl;
    fout << "edge from: to" << endl;
    for (auto a_used : used) 
      fout << a_used.first + num_entities << ": " << a_used.second << endl;
    for (auto a_gen : was_generated_by) 
      fout << a_gen.first << ": " << a_gen.second + num_entities << endl;
  }

  fout.close();
  gsl_rng_free(r);
  return 0;
}
