#include <cmath>
#include <ctime>
#include <iostream>
#include <fstream>
#include <random>
#include <set>
#include <string>
#include <vector>

#include <gsl/gsl_sf_zeta.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

using namespace std;

// when one doesn't know the population
// assume the population is infinity, generalized harmonic number is 
// approximated as zeta function
double get_pdf_zipf(int rank, double skew) {
  return pow(rank, -1 * skew) / gsl_sf_zeta(skew);
}

// when one knows the population
// calculate the generalized harmonic number
double get_pdf_zipf(int rank, int N, double skew) {
  double base = 0;
  for (int k = 1; k <= N; k++) base += pow(k, -1 * skew);
  return pow(rank, -1 * skew) / base;
}

// given N size array, generate the zipf using the array id as rank (variable)
void generate_popularity(double* popularity, int n, double skew = 2.0) {
  for (int rank = 1; rank <= n; rank++) {
    popularity[rank - 1] = get_pdf_zipf(rank, n, skew);
  }
}

// choose an existing entity as its ancestor
int gen_derivation(int num_entities) { 
  return rand() % num_entities; 
}

// smaller skew means, older entities got higher probability to be chosen
// larger skew means, the latest entities are dominant  
set<int> gen_input_entities_zipf(int num_entities, int size, double skew) {
  set<int> result;
  if (size == 0) return result;
  // popularity for existing entities, the later entities, got higher rank
  double popularities[num_entities];
  for (int i = 0; i < num_entities; i++) {
    int rank = num_entities - i;
    popularities[i] = get_pdf_zipf(rank, skew); // use the infinity population
  }
  // create a fresh generator & distribution all the time
  gsl_rng* input_entities_generator = gsl_rng_alloc(gsl_rng_mt19937);
  gsl_ran_discrete_t* input_entities_distr = 
      gsl_ran_discrete_preproc(num_entities, popularities);
  while (result.size() < size) {
    int e = gsl_ran_discrete(input_entities_generator, input_entities_distr);
    result.insert(e);
  }
  gsl_ran_discrete_free(input_entities_distr);
  gsl_rng_free(input_entities_generator);
  return result;
}

// reuse the computed rank to generate large dataset faster
set<int> gen_input_entities_zipf_cached(
  int num_entities, int size, double skew, vector<double>& popularities) {
  set<int> result;
  if (size == 0) return result;
  // popularity for existing entities, the later entities, got higher rank
  int prev_size = popularities.size();
  // added new element will shift the previous rank
  // popularities vector maintain the reverse rank
  for (int rank = prev_size + 1; rank <= num_entities; rank++) {
    popularities.push_back(get_pdf_zipf(rank, skew));
  }
  gsl_rng* input_entities_generator = gsl_rng_alloc(gsl_rng_mt19937);
  gsl_ran_discrete_t* input_entities_distr = 
      gsl_ran_discrete_preproc(num_entities, popularities.data());
  while (result.size() < size) {
    int e = gsl_ran_discrete(input_entities_generator, input_entities_distr);
    result.insert(num_entities - 1 - e);
  }
  gsl_ran_discrete_free(input_entities_distr);
  gsl_rng_free(input_entities_generator);
  return result;
}

void write_nodes(string filepath, string head, int num_nodes) {
  fstream out;
  out.open(filepath, fstream::out); 
  out << head << endl;
  for (int n = 0; n < num_nodes; n++) out << n << endl;
  out.close();
}

void write_edges(string filepath, string head, vector<pair<int,int>>& edges) {
  fstream out;
  out.open(filepath, fstream::out); 
  out << head << endl;
  for (auto edge : edges) out << edge.first << "," << edge.second << endl;
  out.close();
}

int main(int argc, char** argv) {
  if (argc < 3) {
    cout << "usage: ./sd1 N out_path [in_skew] [in_degree]" << endl; 
    return 0;
  }

  // configuration parameters  
  int node_size = stoi(argv[1]); 
  string path(argv[2]);
  if (path.back() != '/') path += "/";

  int input_mean = 2;
  int output_mean = 2;
  double agent_skew = 1.2;
  double in_skew = 1.5;
  double new_version_chance = 0.5;

  if (argc == 4) {
    in_skew = stod(argv[3]);
  }

  if (argc == 5) {
    input_mean = stoi(argv[4]);
  }

  // init 
  int num_activities = node_size / (1 + 1 + output_mean); 
  int num_agents = floor(log(node_size)); 
  int num_entities = 0;

  // activity - input poisson mean
  default_random_engine input_num_generator(time(0));
  poisson_distribution<int> input_num_distr(input_mean);

  // activity - output poisson mean
  default_random_engine output_num_generator(time(0) + rand());
  poisson_distribution<int> output_num_distr(output_mean);

  // agent - work rate,
  double agent_work_rate[num_agents];
  generate_popularity(agent_work_rate, num_agents, agent_skew);
  
  // agent - generator and distr using the work rate
  gsl_rng* agent_generator = gsl_rng_alloc(gsl_rng_mt19937);
  gsl_ran_discrete_t* agent_distr =
      gsl_ran_discrete_preproc(num_agents, agent_work_rate);
  // cached
  vector<double> popularities;      

  // output
  vector<pair<int, int>> used;                 // A x E
  vector<pair<int, int>> was_generated_by;     // E x A
  vector<pair<int, int>> was_attribued_to;     // E x U
  vector<pair<int, int>> was_associated_with;  // A x U
  vector<pair<int, int>> was_derived_from;     // E x E

  // for each activity, random gen input/output degrees from the poissons
  for (int i = 0; i < num_activities; i++) {
    int curr_activity = i;
    // don't allow input or output size = 0, enforce to grow chain
    int input_size = 1 + input_num_distr(input_num_generator); 
    int output_size = 1 + output_num_distr(output_num_generator);
    int curr_agent = gsl_ran_discrete(agent_generator, agent_distr);

    was_associated_with.push_back(make_pair(curr_activity, curr_agent));

    // in the beginning, num_entities would be < input_size
    if (num_entities < input_size) num_entities = input_size; 

    // pick input entities
    auto in_es = gen_input_entities_zipf_cached(
                    num_entities, input_size, in_skew, popularities);
    for (int input_entity : in_es) {
      used.push_back(make_pair(curr_activity, input_entity));
    }

    // pick output entities
    int output_entity = num_entities;
    for (; output_entity < num_entities + output_size; output_entity++) {
      was_generated_by.push_back(make_pair(output_entity, curr_activity));
      was_attribued_to.push_back(make_pair(output_entity, curr_agent));

      // half chance, is a derived entity
      if (rand() % 2 == 0) {
        int parent_entity = gen_derivation(num_entities);
        was_derived_from.push_back(make_pair(output_entity, parent_entity));
      }
    }
    num_entities += output_size;
  }
  gsl_rng_free(agent_generator);

  // write output
  write_nodes(path + "entities.csv", "eid:ID(Entity-ID)", num_entities);
  write_nodes(path + "activities.csv", "aid:ID(Activity-ID)", num_activities);
  write_nodes(path + "agents.csv", "uid:ID(Agent-ID)", num_agents);

  write_edges(path + "used_rels.csv",
              ":START_ID(Activity-ID),:END_ID(Entity-ID)",  used);
  write_edges(path + "gen_rels.csv",
              ":START_ID(Entity-ID),:END_ID(Activity-ID)",  was_generated_by);
  write_edges(path + "attr_rels.csv",
              ":START_ID(Entity-ID),:END_ID(Agent-ID)",  was_attribued_to);
  write_edges(path + "ass_rels.csv",
              ":START_ID(Activity-ID),:END_ID(Agent-ID)",  was_associated_with);
  write_edges(path + "der_rels.csv",
              ":START_ID(Entity-ID),:END_ID(Entity-ID)",  was_derived_from);
  return 0;
}
